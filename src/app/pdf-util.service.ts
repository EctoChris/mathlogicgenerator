import { Injectable } from '@angular/core';
import { FruitPaths } from '../Models/fruitpaths';
//Pdf make import:
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import * as _ from 'lodash';
import { withModule } from '@angular/core/testing';

@Injectable({
  providedIn: 'root'
})

export class PdfUtilService {

  public fruitPaths:any = new FruitPaths();
  public pdfObj = null;
  public pdfObjSolutions = null;
  public finalCustomNumbers = [];
  public pages:any = [];
  public docDefinition:any = { content: [], background: {} };

  constructor() { }

  public downloadBook(canvasArray, canvasSolutionsArray, mergeFiles, solutionsPerPage){
    let initialCanvasLength:number = canvasArray.length;
    //Fill in empty canvas to make even:
    canvasArray = this.fillInArray(solutionsPerPage, canvasArray);
    canvasSolutionsArray = this.fillInArray(solutionsPerPage,  canvasSolutionsArray);

    if(!mergeFiles){

      //Populate Puzzle Answers:
      let counter:number = 1;
      for(let i:number = 0; i < canvasSolutionsArray.length; i++){
        let tempAnswerArray:any = [];
        switch(solutionsPerPage){
          case 2: {
            tempAnswerArray = [canvasSolutionsArray[i], canvasSolutionsArray[i+1]];
            break;
          }
          case 4: {
            tempAnswerArray = [canvasSolutionsArray[i], canvasSolutionsArray[i+1], canvasSolutionsArray[i+2], canvasSolutionsArray[i+3]];
            break;
          }
          case 6: {
            tempAnswerArray = [canvasSolutionsArray[i], canvasSolutionsArray[i+1], canvasSolutionsArray[i+2], canvasSolutionsArray[i+3], canvasSolutionsArray[i+4], canvasSolutionsArray[i+5]];
            break;
          }
        }
        //Generate one PDF Answer
        this.generateAnswerPage(tempAnswerArray, this.fruitPaths.pageBorder, solutionsPerPage, counter);
        console.log('ran ', i);
        i += (solutionsPerPage - 1); //solutionsPerPage - 1 = old
        counter ++;
      }

      let puzzleCounter:number = 1;
      for(let i:number = 0; i <= canvasArray.length; i++){
        console.log('number : '+ i);      
        this.generatePuzzlePage(canvasArray[i], canvasArray[i+1], this.fruitPaths.pageBorder, puzzleCounter);
        i += 1;
        puzzleCounter++;
      }

    }

    if(mergeFiles){
      //Declare empty object:      
      var dd = {
        content: [], 
        background: {}
      }

      //Populate Puzzle Questions:
      for(let i:number = 0; i < canvasArray.length; i++){
        if (i % 2 == 0 && i != 0){
          dd.content.push(this.pageBreak());
        }
        dd.content.push(this.table(canvasArray[i]));
      }

      if(solutionsPerPage == 4 && initialCanvasLength < 4){
        dd.content.push(this.pageBreakAfter());
      }
      if(solutionsPerPage == 6 && initialCanvasLength < 6){
        dd.content.push(this.pageBreak());
      } 

      //Populate Puzzle Answers:
      for(let i:number = 0; i < canvasSolutionsArray.length; i++){
        let tempAnswerArray:any = [];
        let canvasWidth:number = 220;
        let canvasHeight:number = 210;

        switch(solutionsPerPage){
          case 2: {
            tempAnswerArray = [canvasSolutionsArray[i], canvasSolutionsArray[i+1]];
            canvasWidth = 450;
            canvasHeight = 350;

            //Insert Page Break Before:
            dd.content.push(this.pageBreak());
          
            // Canvas 1:
            dd.content.push(this.answerTemplate2(tempAnswerArray[0], canvasWidth, canvasHeight, true));
            // Canvas 2:
            dd.content.push(this.answerTemplate2(tempAnswerArray[1], canvasWidth, canvasHeight, false));
            break;
          }
          case 4: {
            canvasWidth = 245;
            tempAnswerArray = [canvasSolutionsArray[i], canvasSolutionsArray[i+1], canvasSolutionsArray[i+2], canvasSolutionsArray[i+3]];

            //Insert Page Break Before:
            if(i % 4 == 0 && i != 0){
              dd.content.push(this.pageBreak());
            }
            
            // Canvas 1 + 2
            dd.content.push(this.answerTemplate4(tempAnswerArray[0], tempAnswerArray[1], canvasWidth, canvasHeight));
            // Canvas 3 + 4
            dd.content.push(this.answerTemplate4(tempAnswerArray[2], tempAnswerArray[3], canvasWidth, canvasHeight));
            break;
          }
          case 6: {
            canvasWidth = 245;
            tempAnswerArray = [canvasSolutionsArray[i], canvasSolutionsArray[i+1], canvasSolutionsArray[i+2], canvasSolutionsArray[i+3], canvasSolutionsArray[i+4], canvasSolutionsArray[i+5]];

            //Insert Page Break Before:
            if(i % 6 == 0 && i != 0 ){
              dd.content.push(this.pageBreak());
            }
            // Canvas 1 + 2:
            dd.content.push(this.answerTemplate6(tempAnswerArray[0], tempAnswerArray[1], canvasWidth, canvasHeight, 40));
            // Canvas 2 + 3:
            dd.content.push(this.answerTemplate6(tempAnswerArray[2], tempAnswerArray[3], canvasWidth, canvasHeight, 25));
            // Canvas 4 + 5:
            dd.content.push(this.answerTemplate6(tempAnswerArray[4], tempAnswerArray[5], canvasWidth, canvasHeight, 25));
            break;
          }
        }
        i += (solutionsPerPage - 1);
      }
      dd.background = {
        image: this.fruitPaths.pageBorder,
        width: 595,
        height: 841,
        margin: [0, 0, 0, 0]
      }

      let bookPDF = pdfMake.createPdf(dd);
      bookPDF.download('book');
    } 
  }

  public generateAnswerPage(tempAnswerArray, image2, solutionsPerPage, counter){
    let canvasWidth:number = 220;
    let canvasHeight:number = 210;
    var page = null;
    switch(solutionsPerPage){
      case 2:{
        canvasWidth = 450;
        canvasHeight = 350;
         page = {
          content: [
            // Canvas 1
            this.answerTemplate2(tempAnswerArray[0], canvasWidth, canvasHeight, true),
            // Canvas 2
            this.answerTemplate2(tempAnswerArray[1], canvasWidth, canvasHeight, false),
          ],
          //==============================================
          background: function(currentPage, pageSize) {
            console.log('width: ' + pageSize.width);
            console.log('height: ' + pageSize.height);
            return {
              image: image2,
              width: pageSize.width,
              height: pageSize.height,
              margin: [0, 0, 0, 0]
            }
            // return `page ${currentPage} with size ${pageSize.width} x ${pageSize.height}`
          }
        }
        break;
      }
      case 4: {
        canvasWidth = 245;
         page = {
          content: [
            // Canvas 1 + 2
           this.answerTemplate4(tempAnswerArray[0], tempAnswerArray[1], canvasWidth, canvasHeight),
            // Canvas 3 + 4
           this.answerTemplate4(tempAnswerArray[2], tempAnswerArray[3], canvasWidth, canvasHeight),
          ],
          //==============================================
          background: function(currentPage, pageSize) {
            console.log('width: ' + pageSize.width);
            console.log('height: ' + pageSize.height);
            return {
              image: image2,
              width: pageSize.width,
              height: pageSize.height,
              margin: [0, 0, 0, 0]
            }
          }
        }
        break;
      }
      case 6: {
        canvasWidth = 245;
        page = {
         content: [
          // Canvas 1 + 2:
          this.answerTemplate6(tempAnswerArray[0], tempAnswerArray[1], canvasWidth, canvasHeight, 40),
          // Canvas 2 + 3:
          this.answerTemplate6(tempAnswerArray[2], tempAnswerArray[3], canvasWidth, canvasHeight, 25),
          // Canvas 4 + 5:
          this.answerTemplate6(tempAnswerArray[4], tempAnswerArray[5], canvasWidth, canvasHeight, 25),
         ],
         //==============================================
         background: function(currentPage, pageSize) {
           console.log('width: ' + pageSize.width);
           console.log('height: ' + pageSize.height);
           return {
             image: image2,
             width: pageSize.width,
             height: pageSize.height,
             margin: [0, 0, 0, 0]
           }
         }
       }
       break;
     }
    }
   
    this.pdfObj = pdfMake.createPdf(page);
    this.pdfObj.download('Answer ' + counter);
  }

  public pageBreak(){
    return {
        text: '',
        pageBreak: "before" // or after
    }
  }

  public pageBreakAfter(){
    return {
      text: '',
      pageBreak: "after"
    }
  }

  public table(canvas) {
    if(canvas == null)
     return;
    return {
      table: {
        widths: ['0.0000001%', '*', '*'],
        body:[
          [
            {
                text: ''
            },
            {
              image: canvas, 
              width: 350,
              height: 360,
              margin: [75, 10, 0, 0]
            },
            {
               text: ''
            },
          ]
        ],
      },
      layout: 'noBorders',
    }
}

public answerCanvas(canvas, width, height, marginLeft, marginTop){

  if(canvas == null){
    return {
      text: '',
      width: width,
      height: height,
      margin: [0, marginTop, 0, 0]
    }
  }
  
  return {
    image: canvas, 
    width: width,
    height: height,
    margin: [marginLeft, marginTop, 0, 0]
  }
}

public answerTemplate2(canvas, canvasWidth, canvasHeight, isLeft){
  let marginTop:number = 10;
  if(!isLeft){
    marginTop = 15;
  }
  return {
      style: 'section d',
       table: {
        widths: ['0.0000001%', '*', '*'],
        body:[
          [
            {
                text: ''
            },
             this.answerCanvas(canvas, canvasWidth, canvasHeight, 25, marginTop),
            {
               text: ''
            },
          ]
        ]
      },
      layout: 'noBorders'
  }
}

public answerTemplate4(canvas1, canvas2, canvasWidth, canvasHeight){
  return {
      table: {
       widths: ['0.0000001%', '*', '*', '*'],
       body:[
         [
           {
               text: ''
           },
           this.answerCanvas(canvas1, canvasWidth, canvasHeight, 0, 105),
           this.answerCanvas(canvas2, canvasWidth, canvasHeight, 0, 105),
           {
              text: ''
           },
         ]
       ]
     },
     layout: 'noBorders'
   }
}

public answerTemplate6(canvas1, canvas2, canvasWidth, canvasHeight, marginTop){
    return {
      table: {
       widths: ['0.0000001%', '*', '*', '*'],
       body:[
         [
           {
               text: ''
           },
           this.answerCanvas(canvas1, canvasWidth, canvasHeight, 0, marginTop),
           this.answerCanvas(canvas2, canvasWidth, canvasHeight, 0, marginTop),
           {
              text: ''
           },
         ]
       ]
     },
     layout: 'noBorders'
   }
}

  public generatePuzzlePage(canvas1, canvas2, image2, counter){
    var page = {
      content: [
        // Canvas 1
         {
          style: 'section d',
           table: {
            widths: ['0.0000001%', '*', '*'],
            body:[
              [
                {
                    text: ''
                },
                this.answerCanvas(canvas1, 350, 360, 75, 10),
        //         {
        //           image: canvas1, 
        //           width: 350,
        //           height: 360,
        //           margin: [75, 10, 0, 0]
        //         },
                {
                   text: ''
                },
              ]
            ]
          },
          layout: 'noBorders'
        },
         // Canvas 2
         {
          style: 'section',
           table: {
            widths: ['0.0000001%', '*', '*'],
            body:[
              [
                {
                    text: ''
                },
                this.answerCanvas(canvas2, 350, 360, 75, 15),
                {
                   text: ''
                },
              ]
            ]
          },
          layout: 'noBorders'
        },
      ],
      //==============================================
      background: function(currentPage, pageSize) {
        console.log('width: ' + pageSize.width);
        console.log('height: ' + pageSize.height);
        return {
          image: image2,
          width: pageSize.width,
          height: pageSize.height,
          margin: [0, 0, 0, 0]
        }
        // return `page ${currentPage} with size ${pageSize.width} x ${pageSize.height}`
      },
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          background: 'red',
        },
        date: {
          fontSize: 15,
          bold: false,
          color: 'white',
          alignment: 'center',
        },
        section: {
          // fillColor: '#2BA8F1'
        },
        subheader: {
          fontSize: 14,
          bold:true,
          margin: [10, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'center',
          width: '50%',
        }
      }
    }

    // this.pages.push(page.content);
    this.pdfObj = pdfMake.createPdf(page);
    console.log('the working pdf: ');
    console.log(this.pdfObj);
    this.pdfObj.download('Puzzle No ' + counter);
  }

  public fillInArray(answersPerPage, canvasArray){
      //Fill in empty solutions canvas:
      while(canvasArray.length % answersPerPage != 0){
        canvasArray.push(null);
      }
      return canvasArray;
  }
}


export default class Page {
  public topPage:any;
  public bottomPage:any;
  constructor(topPage, bottomPage){
    this.topPage = _.cloneDeep(topPage);
    this.bottomPage = _.cloneDeep(bottomPage);
  }
}