import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { LetterUtilService } from '../letter-util.service'
import { PdfUtilService } from '../pdf-util.service';
import asyncLoop from 'node-async-loop';

//Image Local URLS:
import IMAGE from '../../imageSets'

//Models:
import { Fruit } from '../../Models/fruit';
import { Equation } from '../../Models/equation';

enum Difficulty {
  JUNIOR = "JUNIOR",
  MEDIUM = "MEDIUM",
  HARD = "HARD"
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  @ViewChild('canvas', {static: true}) canvasEl : ElementRef;
  @ViewChild ('canvasSolution', {static: true}) canvasSolutionEl :ElementRef;

  //UI Data Gathering:
  private canvas: any;
  private canvasSolution: any;
  private outputCanvas: any;
  private outputSolutionCanvas: any;
  private ctx : any;
  private ctx2: any;
  private lineWidth: number = 2;
  public puzzlesToDownload:number = 4;
  public solutionsPerPage:number = 4;
  public canvasArray:any = [];
  public canvasSolutionsArray:any = [];
  public checkbox:boolean = true;

  //Puzzle Difficulty Title:
  private title:any;

  //FruitObjects:
  private fruitImageArray:any =        [IMAGE.FRUITS.APPLE.SINGLE, IMAGE.FRUITS.BANANA.SINGLE, IMAGE.FRUITS.BLUEBERRY.SINGLE, IMAGE.FRUITS.CHERRY.SINGLE, IMAGE.FRUITS.GRAPES.SINGLE, IMAGE.FRUITS.KIWI.SINGLE, IMAGE.FRUITS.LEMON.SINGLE, IMAGE.FRUITS.ORANGE.SINGLE, IMAGE.FRUITS.PEAR.SINGLE, IMAGE.FRUITS.PINEAPPLE.SINGLE, IMAGE.FRUITS.STRAWBERRY.SINGLE, IMAGE.FRUITS.WATERMELON.SINGLE];
  private fruitImageArrayDoubles:any = [IMAGE.FRUITS.APPLE.DOUBLE, IMAGE.FRUITS.BANANA.DOUBLE, IMAGE.FRUITS.BLUEBERRY.DOUBLE, IMAGE.FRUITS.CHERRY.DOUBLE, IMAGE.FRUITS.GRAPES.DOUBLE, IMAGE.FRUITS.KIWI.DOUBLE, IMAGE.FRUITS.LEMON.DOUBLE, IMAGE.FRUITS.ORANGE.DOUBLE, IMAGE.FRUITS.PEAR.DOUBLE, IMAGE.FRUITS.PINEAPPLE.DOUBLE, IMAGE.FRUITS.STRAWBERRY.DOUBLE, IMAGE.FRUITS.WATERMELON.DOUBLE];
  private fruitImageArrayTriples:any = [IMAGE.FRUITS.APPLE.TRIPLE, IMAGE.FRUITS.BANANA.TRIPLE, IMAGE.FRUITS.BLUEBERRY.TRIPLE, IMAGE.FRUITS.CHERRY.TRIPLE, IMAGE.FRUITS.GRAPES.TRIPLE, IMAGE.FRUITS.KIWI.TRIPLE, IMAGE.FRUITS.LEMON.TRIPLE, IMAGE.FRUITS.ORANGE.TRIPLE, IMAGE.FRUITS.PEAR.TRIPLE, IMAGE.FRUITS.PINEAPPLE.TRIPLE, IMAGE.FRUITS.STRAWBERRY.TRIPLE, IMAGE.FRUITS.WATERMELON.TRIPLE];

  private fruitArray:any = [];

  //Equations:
  private equations:any = [];

  //Difficulty:
  private difficulty:Difficulty = Difficulty.HARD;
  private doubleFruitCount:number = 0;

  constructor(private LU:LetterUtilService, private PDF:PdfUtilService, private router: Router) {}

  ngOnInit(){
    this.createPuzzle();
  }

  public downloadPDF(){
    this.canvasArray = [];
    this.canvasSolutionsArray = [];
    console.log(this.puzzlesToDownload);
    let obj:any = [];
    for(let i:number = 1; i <= this.puzzlesToDownload; i++)
    {
      obj.push(i);
    }
    asyncLoop(obj, async (i, next) =>
    {
      this.createPuzzle();
      this.drawPuzzleNumber(this.ctx, i);
      this.drawPuzzleNumber(this.ctx2, i);
      await this.delay(500);

      this.outputCanvas = this.canvas.toDataURL();
      this.outputSolutionCanvas = this.canvasSolution.toDataURL();
      this.canvasArray.push(this.outputCanvas);
      this.canvasSolutionsArray.push(this.outputSolutionCanvas);
      // this.PDF.generatePage(this.outputCanvas, )
      // this.PDF.downloadPdf(this.outputCanvas, this.outputSolutionCanvas, 'Sudoku', i);  
      if(i == obj.length){
        await this.delay(500);
        this.download(this.checkbox);
      }
      next();
    }, function ()
    {
      console.log('Finished!');
    });
  }

  download(mergeFiles){
    this.PDF.downloadBook(this.canvasArray, this.canvasSolutionsArray, mergeFiles, this.solutionsPerPage);
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
  public createPuzzle(){
    this.refreshUI();
    this.setupCanvas();
    this.generateFruits();
    this.generateEquations();
    this.drawEquations();
  }

  public goToPatternPuzzlePage(){
    this.router.navigate(['/pattern'])
  }

  public goToWordPuzzlePage(){
    this.router.navigate(['/word'])
  }

  public generateFruits(){
    console.log(this.checkbox + ' is the status');
    let usedNumbers:any = [];
    for(let i:number = 0; i < 3; i++)
    {
      //Setup empty image object:
      let image = new Image();

      //Randomly pick fruit picture:
      let r:number = Math.floor(Math.random() * this.fruitImageArray.length);
      while(usedNumbers.includes(r)){
         r = Math.floor(Math.random() * this.fruitImageArray.length);
      }
      let src:string = this.fruitImageArray[r];
      image.src = src;

      //Create fruit object:
      let fruit:Fruit = new Fruit(image, this.difficulty, r, 0, false, false);
      console.log(fruit.image.src);

      //Add Fruit object to fruitarray:
      this.fruitArray.push(fruit);

      //Add to used fruits:
      usedNumbers.push(r);
    }
  }

  public setActiveDifficulty(num){
    switch(num){
      case 1: {
        this.difficulty = Difficulty.JUNIOR;
        break;
      }
      case 2: {
        this.difficulty = Difficulty.MEDIUM;
        break;
      }
      case 3: {
        this.difficulty = Difficulty.HARD;
        break;
      }
    }
    this.createPuzzle();
  }

  public generateEquations(){
    if(this.difficulty == Difficulty.JUNIOR){
      //Equation One:
      let equationOneFruits:any = [this.fruitArray[0], this.fruitArray[0]];
      let equationOne:Equation = new Equation(equationOneFruits, this.difficulty, true,  false, false);

      //Equation Two:
      let equationTwoFruits:any = [this.fruitArray[0], this.fruitArray[1]];
      equationTwoFruits = this.LU.sortArrayDescending(equationTwoFruits);
      let equationTwo:Equation = new Equation(equationTwoFruits, this.difficulty, false, true, false);

      //Equation Three:
      let equationThreeFruits:any = [this.fruitArray[1], this.fruitArray[2]];
      equationThreeFruits = this.LU.sortArrayDescending(equationThreeFruits);
      let equationThree:Equation = new Equation(equationThreeFruits, this.difficulty, false, true, false);
      
      //Equation Four (Problem):
      let equationFourFruits:any = [this.fruitArray[0], this.fruitArray[2]];
      equationFourFruits = this.LU.sortArrayDescending(equationFourFruits);
      let equationFour:Equation = new Equation(equationFourFruits, this.difficulty, false, false, true);

      //Add all to equations:
      this.equations.push(equationOne, equationTwo, equationThree, equationFour);
    } else if (this.difficulty == Difficulty.MEDIUM){
      //Equation One:
      let equationOneFruits:any = [this.fruitArray[0], this.fruitArray[0]];
      equationOneFruits = this.randomlyChangeFruitToDouble(equationOneFruits);
      let equationOne:Equation = new Equation(equationOneFruits, this.difficulty, true, false,false);

      //Equation Two:
      let equationTwoFruits:any = [this.fruitArray[0], this.fruitArray[1]];
      equationTwoFruits = this.randomlyChangeFruitToDouble(equationTwoFruits);
      // equationTwoFruits = this.LU.sortArrayDescending(equationTwoFruits);
      let equationTwo:Equation = new Equation(equationTwoFruits, this.difficulty, false,true, false);

      //Equation Three:
      let equationThreeFruits:any = [this.fruitArray[1], this.fruitArray[2]];
      equationThreeFruits = this.randomlyChangeFruitToDouble(equationThreeFruits);
      // equationThreeFruits = this.LU.sortArrayDescending(equationThreeFruits);
      let equationThree:Equation = new Equation(equationThreeFruits, this.difficulty, false, true,false);

      //Equation Four (Problem):
      let equationFourFruits:any = [this.fruitArray[0], this.fruitArray[2]];
      equationFourFruits = this.randomlyChangeFruitToDouble(equationFourFruits);
      let equationFour:Equation = new Equation(equationFourFruits, this.difficulty, false, false, true);

      //Add all to equations:
      this.equations.push(equationOne, equationTwo, equationThree, equationFour);
    } else if (this.difficulty == Difficulty.HARD){
      //Equation One:
      let equationOneFruits:any = [this.fruitArray[0], this.fruitArray[0], this.fruitArray[0]];
      equationOneFruits = this.randomlyChangeFruitToDoubleOrTriple(equationOneFruits);
      let equationOne:Equation = new Equation(equationOneFruits, this.difficulty, true,false, false);

      //Equation Two:
      let equationTwoFruits:any = [this.fruitArray[0], this.fruitArray[1]];
      let r:number = Math.floor(Math.random() * 2);
      if(r == 1)
        equationTwoFruits.push(this.fruitArray[0]);
      else
        equationTwoFruits.push(this.fruitArray[1]);
      
      equationTwoFruits = this.randomlyChangeFruitToDoubleOrTriple(equationTwoFruits);
      let equationTwo:Equation = new Equation(equationTwoFruits, this.difficulty, false, true, false);

      //Equation Three:
      let equationThreeFruits:any = [this.fruitArray[1], this.fruitArray[2]];
      r = Math.floor(Math.random() * 2);
      if(r == 1)
        equationThreeFruits.push(this.fruitArray[1]);
      else
        equationThreeFruits.push(this.fruitArray[2]);

      equationThreeFruits = this.LU.shuffle(equationThreeFruits);
      equationThreeFruits = this.randomlyChangeFruitToDoubleOrTriple(equationThreeFruits);
      let equationThree:Equation = new Equation(equationThreeFruits, this.difficulty, false, true, false);

      //Equation Four (Problem):
      let equationFourFruits:any = [this.fruitArray[0], this.fruitArray[2]];
      r = Math.floor(Math.random() * 2);
      if(r == 1)
        equationFourFruits.push(this.fruitArray[0]);
      else
        equationFourFruits.push(this.fruitArray[2]);

      equationFourFruits = this.randomlyChangeFruitToDoubleOrTriple(equationFourFruits);
      let equationFour:Equation = new Equation(equationFourFruits, this.difficulty, false, false, true);

      //Add All to equations:
      this.equations.push(equationOne, equationTwo, equationThree, equationFour);
    }
  }

  public refreshUI(){
    this.canvas	        = this.canvasEl.nativeElement;
    this.canvas.width   = 875;
    this.canvas.height  = 925;
    this.canvasSolution = this.canvasSolutionEl.nativeElement;
    this.canvasSolution.width = 875;
    this.canvasSolution.height = 675;
    this.equations = [];
    this.fruitArray = [];
  }

  setupCanvas(){
    this.ctx = this.canvas.getContext('2d');
    this.ctx.lineWidth = this.lineWidth;
    this.ctx.font = " bold 80px Arial";

    this.ctx2 = this.canvasSolution.getContext('2d');
    this.ctx2.lineWidth = this.lineWidth;
    this.ctx2.font = "bold 80px Arial";

    this.drawBorderOnCanvas(this.ctx, this.canvas);
    this.drawBorderOnCanvas(this.ctx2, this.canvasSolution);
  }

  drawEquations(){
    let yPadding:number = 340;
    let yPos:number = 220;
    let xPos:number = 100;
    switch(this.difficulty){
      case Difficulty.JUNIOR: {

        for(let i:number = 0; i < this.equations.length; i++){
          this.drawJuniorEquation(this.ctx, this.equations[i], xPos, yPos, false);
          yPos += 170;
        }
        yPos = 110;
        this.drawJuniorEquation(this.ctx2, this.equations[this.equations.length-1], xPos, yPos + yPadding, true);
        this.drawTitle(this.ctx, false);
        this.drawTitle(this.ctx2, true);
        break;
      }
      case Difficulty.MEDIUM: {
        for(let i:number = 0; i < this.equations.length; i++){
          this.drawJuniorEquation(this.ctx, this.equations[i], xPos, yPos, false);
          yPos += 170;
        }
        yPos = 110;
        this.drawJuniorEquation(this.ctx2, this.equations[this.equations.length-1], xPos, yPos + yPadding, true);
        this.drawTitle(this.ctx, false);
        this.drawTitle(this.ctx2, true);
        break;
      }
      case Difficulty.HARD: {
        let xPos:number = 70;
        for(let i:number = 0; i < this.equations.length; i++){
          this.drawHardEquation(this.ctx, this.equations[i], xPos, yPos, false);
          yPos += 170;
        }
        yPos = 110;
        this.drawHardEquation(this.ctx2, this.equations[this.equations.length-1], xPos, yPos + yPadding, true);
        this.drawTitle(this.ctx, false);
        this.drawTitle(this.ctx2, true);
        break;
      }
    }
  }

  drawJuniorEquation(ctx, equation, xPos, yPos, drawingSolution){
    let originalXPos = xPos;
    let originalYPos = yPos;
    //Drawing Objects:
    for(let i:number = 0; i < equation.objects.length; i++){
      this.drawImage(ctx, equation.objects[i].image.src, xPos, yPos,false);
      xPos += 500 / equation.objects.length;
    }

    originalXPos += 170 / equation.operators.length;
    originalYPos += 100;

    //Drawing Operators:
    for(let i:number = 0; i < equation.operators.length; i++){
      this.drawText(ctx, equation.operators[i], originalXPos, originalYPos);
    }

    //Draw '=':
    this.drawText(ctx, '=', originalXPos+ 250, originalYPos);

    //Draw Answer:
    if(equation.isProblem == false)
        this.drawText(ctx, equation.answer, originalXPos + 380, originalYPos);
    else if (drawingSolution){
      this.drawText(ctx, equation.answer, originalXPos + 380, originalYPos);
    } else {
      this.drawText(ctx, '__',originalXPos + 375, originalYPos);
    }
  }

  drawHardEquation(ctx, equation, xPos, yPos, drawingSolution){
    let originalXPos = xPos;
    let originalYPos = yPos;
    //Drawing Objects:
    for(let i:number = 0; i < equation.objects.length; i++){
      this.drawImage(ctx, equation.objects[i].image.src, xPos, yPos, true);
      xPos += 600 / equation.objects.length;
    }

    originalYPos += 100;

    //Drawing Operators:
    for (let i:number = 0; i < equation.operators.length; i++){
      if(i == 0)
          originalXPos += 240 / equation.operators.length;
      else 
      originalXPos += 370 / equation.operators.length;
      this.drawText(ctx, equation.operators[i], originalXPos, originalYPos);
    }
    
     //Draw '=':
     this.drawText(ctx, '=', originalXPos+ 234, originalYPos);

     //Draw Answer:
     if(equation.isProblem == false){
      this.drawText(ctx, equation.answer, originalXPos + 330, originalYPos);
     }
     else if (drawingSolution){
       this.drawText(ctx, equation.answer, originalXPos + 330, originalYPos);
     } else {
       this.drawText(ctx, '__',originalXPos + 325, originalYPos);
     }
  }

  drawImage(ctx, src, xPos, yPos, isHard){
    let width:number = 140;
    let height:number = 140;
    if(isHard){
      width = height = 120;
      xPos -= 20;
    }
    let image = new Image();
    let context = ctx;
    image.onload = function(){
      context.drawImage(image, xPos, yPos, width, height);
    }
    image.src = src;
    console.log('number: ' + this.fruitArray[0].number);
  }

  drawText(ctx, word, x, y){
    if(word == '-'){
      x += 7;
    }
    if(word == '*'){
      word = 'x';
    }
    ctx.fillText(word, x, y);
  }

  drawPuzzleNo(ctx, num, x, y){
    ctx.font = "bold 35px Arial";
    ctx.fillText(num, x, y);
    ctx.font = " bold 80px Arial";
  }

  drawTitle(ctx, isSolution){
    let xPos:number = 255;
    let yPos:number = 140;
    let image = new Image();
    let imageAnswer = new Image();
    let context = ctx;
    image.onload = function(){
      context.drawImage(image, xPos, yPos, 350, 70);
    }
    if(isSolution){
      let context = ctx;
      imageAnswer.onload = function(){
        context.drawImage(imageAnswer, xPos - 180, yPos + 110, 710, 160);
      }
    }
    switch(this.difficulty){
      case Difficulty.JUNIOR: {
        image.src = "../../assets/titles/Junior.png";
        imageAnswer.src="../../assets/titles/Answer3.png";
        break;
      }
      case Difficulty.MEDIUM: {
        image.src = "../../assets/titles/Medium.png";
        imageAnswer.src="../../assets/titles/Answer3.png";
        break;
      }
      case Difficulty.HARD: {
        image.src= "../../assets/titles/Hard.png";
        imageAnswer.src="../../assets/titles/Answer3.png";
        break;
      }
    }
  }

  drawPuzzleNumber(ctx, puzzleNo){
    let xPos:number = 530;
    let yPos:number = 30;
    let image = new Image();
    let context = ctx;
    image.onload = function(){
      context.drawImage(image, xPos, yPos, 310, 80);
    }
    image.src= "../../assets/general/PuzzleWordImage2.png";
    this.drawPuzzleNo(ctx, puzzleNo, 710, 85);
  }

  public convertFruitToDouble(fruit){
    let image = new Image();
    let src:string = this.fruitImageArrayDoubles[fruit.fruitID];
    image.src = src;
    let doubleFruit:Fruit = new Fruit(image, this.difficulty, fruit.fruitID, fruit.number * 2, true, false);
    doubleFruit.image.src = this.fruitImageArrayDoubles[doubleFruit.fruitID];
    return doubleFruit;
  }

  public convertFruitToTriple(fruit){
    let image = new Image();
    let src:string = this.fruitImageArrayTriples[fruit.fruitID];
    image.src = src;
    let tripleFruit:Fruit = new Fruit(image, this.difficulty, fruit.fruitID, fruit.number * 3, false, true);
    tripleFruit.image.src = this.fruitImageArrayTriples[tripleFruit.fruitID];
    return tripleFruit;
  }

  public randomlyChangeFruitToDouble(fruitArray){
    for(let i:number = 0; i < fruitArray.length; i++){
      let r:number = Math.floor(Math.random() * 6);
      if (r == 2){
        fruitArray[i] =  this.convertFruitToDouble(fruitArray[i]);
        this.doubleFruitCount++;
      }
    }
    return fruitArray;
  }

  public randomlyChangeFruitToDoubleOrTriple(fruitArray){
    for(let i:number = 0; i < fruitArray.length; i++){
      let r:number = Math.floor(Math.random() * 6);
      if (r == 2){
        fruitArray[i] = this.convertFruitToDouble(fruitArray[i]);
      } else if (r == 3){
        fruitArray[i] = this.convertFruitToTriple(fruitArray[i]);
      }
    }
    return fruitArray;
  }

  public drawBorderOnCanvas(ctx, canvas){
    var grd = ctx.createLinearGradient(0, 0, canvas.width+5, 0);
    grd.addColorStop(0, "#3a5968");
    grd.addColorStop(1, "#D16B24");
    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, canvas.width+5, canvas.height);
    ctx.fillStyle = "#FFF";
    ctx.fillRect(15, 15, canvas.width - 30, canvas.height - 30);
    ctx.fillStyle = "#000";
}
public setSolutionsPerPage(num){
  this.solutionsPerPage = num;
}
}
