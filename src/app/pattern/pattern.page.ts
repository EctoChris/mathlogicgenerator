import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-pattern',
  templateUrl: './pattern.page.html',
  styleUrls: ['./pattern.page.scss'],
})
export class PatternPage implements OnInit {

  constructor(public router:Router) { }

  ngOnInit() {
  }

  public goToFruitPuzzle(){
    this.router.navigate(['/home'])
  }

  public goToWordPuzzle(){
    this.router.navigate(['/word'])
  }
}
