import { TestBed } from '@angular/core/testing';

import { PdfUtilService } from './pdf-util.service';

describe('PdfUtilService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PdfUtilService = TestBed.get(PdfUtilService);
    expect(service).toBeTruthy();
  });
});
