import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LetterUtilService {

  constructor() { }

  public shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

   return array;
  }

  public sortArrayDescending(array){
    array.sort((a, b) => parseFloat(a.number) - parseFloat(b.number));
    array.reverse();
    return array;
  }

  public generateRandomNumber(){
    let r = Math.floor(Math.random() * 45);
    return r;
  }

  public outputArray(array){
    let string:string = '';
    for(let i:number = 0; i < array.length; i++){
      string += array[i];
    }
    console.log(string);
  }
}
