import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { Router } from '@angular/router';
import { isFulfilled } from 'q';

//Models:
import { Letter } from '../../Models/letter';
import { Encryption } from '../../Models/encryption';
import { LetterUtilService } from '../letter-util.service';


enum Sentence {
  SHORT = "SHORT",
  MEDIUM = "MEDIUM",
  LONG = "LONG"
}

@Component({
  selector: 'app-word',
  templateUrl: './word.page.html',
  styleUrls: ['./word.page.scss'],
})

export class WordPage implements OnInit {
  @ViewChild('canvas', {static: true}) canvasEl : ElementRef;
  @ViewChild('numberDiv', {static: true}) numberDivEl :ElementRef;
  // @ViewChild ('canvasSolution', {static: true}) canvasSolutionEl :ElementRef;

  //UI Data Gathering:
  private canvas: any;
  private numberDiv: any;
  private canvasSolution: any;
  private ctx : any;
  private ctx2: any;
  private lineWidth: number = 2;
  private boxWidth:number = 45;
  private boxHeight:number = 50;
  private boxPadding:number = 1;

  public word:string = '';
  public encryptedWord:any = [];
  public encryptedSentence:Encryption;
  public encryption:Encryption;
  public number:number = 1;
  public downloadPressed:boolean = false;
  public keys:any = [];

  constructor(public router:Router, public LU:LetterUtilService) {
     //Keyboard Listener:
     document.addEventListener('keyup', (e:KeyboardEvent) => {
      // Entered key is a number (codes for numbers above keyboard and numeric keypad:)
      if((e.keyCode >= 65 && e.keyCode <=90)) { 
          this.word += e.key.toString().toUpperCase();
      } else if (e.keyCode == 8){
        //BackSpace/Delete:
        this.word = '';
        this.encryptedWord = [];
        this.encryptedSentence = null;
        this.encryption = null;
        this.keys = [];
      } else if (e.keyCode == 32){
        e.preventDefault();
        this.word += ' ';
        
      }
      else if (e.keyCode == 13){
        //Enter Key:
        console.log('first number: ' + this.number);
        console.log(e.keyCode);
        e.preventDefault();
        
        this.downloadPressed = true;
        if(this.checkValidWord()){
          //All good, generate puzzle:
          this.generateWordPuzzle();
        }
      }
    });

    window.addEventListener('keydown', function(e) {
      if(e.keyCode == 32 && e.target == document.body) {
        e.preventDefault();
      } else if (e.keyCode == 13 && e.target == document.body){
        e.preventDefault();
      }
    });
  }

  // window.onkeydown = function(e) { 
  //   return !(e.keyCode == 32);
  // };

  ngOnInit() {
    this.refreshUI();
    this.setupCanvas();
  }

  public checkValidWord(){
    if(this.number != 0 && this.word.length > 3){
        return true;
    } else {
        return false;
    }
  }

  public generateWordPuzzle(){
    this.refreshUI();
    this.setupCanvas();
    this.drawTitles();
    this.encryptWord();
    this.drawEncryptedSentence();
    this.drawKeys();
  }

  public encryptWord(){
    //Create array of letters from word (no duplicates):
    let uniqueCharacters:any = [];
    for(let i:number = 0; i < this.word.length; i++){
      if(!uniqueCharacters.includes(this.word[i]) && this.word[i] != ' '){
        uniqueCharacters.push(this.word[i]);
      }
    }

    //Create 'Encryption' for storing encrypted letters:
    this.encryption = new Encryption();
    this.encryptedSentence = new Encryption();

    //Create a 'Letter' model for each unique character:
    let uniqueNumbers:any = [];
    let usedBefore:boolean = true;
    let random:number;

    //Generate Random Number and if it hasn't been used before, add it to a new letter:
    for(let i:number = 0; i < uniqueCharacters.length; i++){
      usedBefore = true;
      while(usedBefore){
        random = this.LU.generateRandomNumber();
        if(!uniqueNumbers.includes(random)){
          usedBefore = false;
          uniqueNumbers.push(random);
        }
      }
      let newLetter:Letter = new Letter(uniqueCharacters[i], random);
      this.encryption.letters.push(newLetter);
    }

    //Encrypt Original Word:
    for(let i:number = 0; i < this.word.length; i++){
      this.encryptedWord.push(this.encryption.encryptLetter(this.word[i]));
    }

    //Encrypt UniqueCharacters:
    for(let i:number = 0; i < uniqueCharacters.length; i++){
      this.keys.push(new Letter(uniqueCharacters[i], this.encryption.encryptLetter(uniqueCharacters[i])));
    }

    //Create Final Encrypted Sentence:
    for(let i:number = 0; i < this.word.length; i++){
      let newLetter:Letter = new Letter(this.word[i], this.encryptedWord[i]);
      this.encryptedSentence.letters.push(newLetter);
    }

    //Output encrypted Sentence:
    for(let i:number = 0; i < this.encryptedSentence.letters.length; i++){
      console.log(this.encryptedSentence.letters[i].char + '    ' + this.encryptedSentence.letters[i].number);
    }

    //Output characters:
    let string:string = '';
    for(let i:number = 0; i < uniqueCharacters.length; i++){
      string += uniqueCharacters[i];
    }
    console.log(string + ' is ' + string.length + ' letters long');
  }

  // endXPos = xPos + (encryptedWords[i].letters.length * (this.boxWidth + this.boxPadding));
  // if(endXPos >= 1150){
  //   xPos = 50;
  //   yPos += 120;
  //   endXPos = xPos + (encryptedWords[i].letters.length * (this.boxWidth+1));
  // }
  // this.drawEncryptedWord(this.ctx, encryptedWords[i], xPos, yPos, Sentence.LONG);
  // xPos += (endXPos - xPos) + (this.boxWidth + this.boxPadding);

  public drawKeys(){
    let xPos:number = 80;
    let yPos:number = 170;
    let endXPos:number = 0;
    let xPadding:number = 0;
    let yPadding:number = 90;
    let xStartPos:number = 80;
    let fontSize:number = 0;
    if(this.keys.length < 10){
      xPadding = 170;
      fontSize = 50;
      xStartPos = 80;
    } else if (this.keys.length >= 10 && this.keys.length <= 20){
      yPos = 140;
      xPadding = 140;
      fontSize = 28;
      yPadding = 60;
    } else if (this.keys.length > 20){
      xPadding = 90;
      fontSize = 25;
    }

    for(let i:number = 0; i < this.keys.length; i++){
      endXPos = xPos + xPadding;
      console.log('initial endxPos: ', endXPos);
      if(endXPos >= 1150){
        xPos = xStartPos;
        yPos += yPadding;
        endXPos = xPos + (this.keys.length * xPadding) + xPadding;
        console.log('final endxPos: ', endXPos);
      }
      console.log('xPos: ',xPos);
      this.drawKey(this.ctx, this.keys[i].char, this.keys[i].number, xPos, yPos, fontSize);
      xPos += xPadding;
    }
  }

  drawEncryptedSentence(){
    let encryptedWords:any = [];
    let encryptedWord:Encryption = new Encryption();
    //Break up encrypted sentence into words:
    for(let i:number = 0; i < this.encryptedSentence.letters.length; i++){
      if(this.encryptedSentence.letters[i].char != ' '){
        encryptedWord.letters.push(this.encryptedSentence.letters[i]);
        if (i == this.encryptedSentence.letters.length-1)
          encryptedWords.push(encryptedWord);
      }
      else {
        encryptedWords.push(encryptedWord);
        encryptedWord = new Encryption();
      }
    }
    console.log(this.word.length + ' is the answer');
    if(this.word.length < 18){
      this.drawShortSentence(encryptedWords);
      
    } else if(this.word.length >= 18 && this.word.length < 27){
      this.drawMediumSentence(encryptedWords);
    }
    else if(this.word.length >= 27 && this.word.length < 67){
      this.drawLongSentence(encryptedWords);
    }
  }

  drawShortSentence(encryptedWords){
    let xPos:number = 100;
    let yPos:number = 300;
    this.boxWidth = 95;
    this.boxHeight = 100;
    this.boxPadding = 5;
    let xPosStart:number = 100;
    let yPosNewLine:number = 200;
    let endXPos:number = 0;

    for(let i:number = 0; i < encryptedWords.length; i++){
      //Step 1: Calculate end xPos of word:
      endXPos = xPos + (encryptedWords[i].letters.length * (this.boxWidth+ this.boxPadding));
      if(endXPos >= 1150){
        xPos = xPosStart;
        yPos += yPosNewLine;
        endXPos = xPos + (encryptedWords[i].letters.length * (this.boxWidth+ this.boxPadding));
      }
      this.drawEncryptedWord(this.ctx, encryptedWords[i], xPos, yPos, Sentence.SHORT);
      xPos += (endXPos - xPos) + (this.boxWidth + this.boxPadding);
    }
  }

  drawMediumSentence(encryptedWords){
    let xPos:number = 50;
    let yPos:number = 300;
    this.boxWidth = 75;
    this.boxHeight = 80;
    this.boxPadding = 3;
    let xPosStart:number = 50;
    let yPosNewLine:number = 200;
    let endXPos:number = 0;

    for(let i:number = 0; i < encryptedWords.length; i++){
      //Step 1: Calculate end xPos of word:
      endXPos = xPos + (encryptedWords[i].letters.length * (this.boxWidth+ this.boxPadding));
      if(endXPos >= 1150){
        xPos = xPosStart;
        yPos += yPosNewLine;
        endXPos = xPos + (encryptedWords[i].letters.length * (this.boxWidth+ this.boxPadding));
      }
      this.drawEncryptedWord(this.ctx, encryptedWords[i], xPos, yPos, Sentence.MEDIUM);
      xPos += (endXPos - xPos) + (this.boxWidth + this.boxPadding);
    }
  }

  drawLongSentence(encryptedWords){
    let xPos:number = 50;
    let yPos:number = 350;
    this.boxWidth = 45;
    this.boxHeight = 50;
    this.boxPadding = 1;
    let endXPos:number = 0;

    for(let i:number = 0; i < encryptedWords.length; i++){
      //Step 1: Calculate end xPos of word:
      endXPos = xPos + (encryptedWords[i].letters.length * (this.boxWidth + this.boxPadding));
      if(endXPos >= 1150){
        xPos = 50;
        yPos += 120;
        endXPos = xPos + (encryptedWords[i].letters.length * (this.boxWidth + this.boxPadding));
      }
      this.drawEncryptedWord(this.ctx, encryptedWords[i], xPos, yPos, Sentence.LONG);
      xPos += (endXPos - xPos) + (this.boxWidth + this.boxPadding);
    }
  }

  drawEncryptedWord(ctx, encryptedWord, x, y, sentenceLength){
    //Step 2: Draw encrypted word in appropriate place:
    for(let i:number = 0; i < encryptedWord.letters.length; i++){
      this.drawEncryptedLetter(ctx, encryptedWord.letters[i], x, y, sentenceLength);
      x += (this.boxWidth + this.boxPadding);
    }
  }

  drawEncryptedLetter(ctx, letter, x, y, sentenceLength){
    if(letter.char == ' ')
      return;

    ctx.rect(x, y, this.boxWidth, this.boxHeight);
    ctx.stroke();

    switch(sentenceLength){
      case Sentence.SHORT: {
        this.drawLetterUnderBox(ctx, letter.number, x + this.boxWidth * 0.4, y+this.boxHeight * 1.5, 35);
        console.log('SHORT BITCH');
        break;
      }
      case Sentence.MEDIUM: {
        this.drawLetterUnderBox(ctx, letter.number, x + this.boxWidth * 0.4, y+this.boxHeight * 1.5, 25);
        console.log('MEDIUM BITCH');
        break;
      }
      case Sentence.LONG: {
        this.drawLetterUnderBox(ctx, letter.number, x + this.boxWidth * 0.4, y+this.boxHeight * 1.5, 20);
        console.log('LONG BITCH');
        break;
      }
    }
  }

  drawLetterUnderBox(ctx, char, x, y, fontSize){
    ctx.font= "bold " + fontSize + "px Arial";
    ctx.fillText(char, x, y);
  }

  drawText(ctx, word, x, y){
    ctx.fillText(word, x, y);
  }

  drawKey(ctx, char, number, x, y, fontSize){
    ctx.font= "bold " + fontSize + "px Arial";
    ctx.fillText(char + ' - ' + number, x, y);
  }

  public refreshUI(){
    this.canvas	        = this.canvasEl.nativeElement;
    this.canvas.width   = 1175;
    this.canvas.height  = 725;
    // this.canvasSolution = this.canvasSolutionEl.nativeElement;
    // this.canvasSolution.width = 875;
    // this.canvasSolution.height = 675;
  }

  setupCanvas(){
    this.ctx = this.canvas.getContext('2d');
    this.ctx.lineWidth = this.lineWidth;
    this.ctx.font = " bold 80px Arial";
    // this.ctx2 = this.canvasSolution.getContext('2d');
    // this.ctx2.lineWidth = this.lineWidth;
    // this.ctx2.font = "bold 80px Arial";
    
    //Draw Border:
    this.drawBorderOnCanvas(this.ctx, this.canvas);
  }

  public drawBorderOnCanvas(ctx, canvas){
    var grd = ctx.createLinearGradient(0, 0, canvas.width+5, 0);
    grd.addColorStop(0, "#3a5968");
    grd.addColorStop(1, "#D16B24");
    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, canvas.width+5, canvas.height);
    ctx.fillStyle = "#FFF";
    ctx.fillRect(15, 15, canvas.width - 30, canvas.height - 30);
    ctx.fillStyle = "#000";
  }


  drawTitles(){
    this.ctx.font  = "bold 65px Arial";
    this.drawText(this.ctx, "Word Puzzle " + this.number, 310, 80);
    // this.drawText(this.ctx2, "Word Puzzle " + this.number, 450, 30);
  }

  //Changing Puzzle Number:
  public add(e)      { this.number++;  e.target.blur(); }
  public subtract(e) { this.number--;  e.target.blur(); }

  //Router Navigation:
  public goToFruitPuzzle(){     this.router.navigate(['/home'])}
  public goToPatternPuzzle(){   this.router.navigate(['/pattern'])}
}
