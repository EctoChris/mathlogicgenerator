enum Difficulty {
    JUNIOR = "JUNIOR",
    MEDIUM = "MEDIUM",
    HARD = "HARD"
}

export class Fruit {
    public number:number;
    public image:any;
    public fruitID:number;

    constructor(image, difficulty, fruitID, number, isDouble, isTriple){
        this.fruitID = fruitID;
        this.number = number;
        this.image = image;

        if(isDouble || isTriple)
            return;

        switch(difficulty){
            case Difficulty.JUNIOR: {
                this.number = Math.ceil(Math.random() * 10);
                break;
            }
            case Difficulty.MEDIUM: {
                this.number = Math.ceil(Math.random() * 10);
                break;
            }
            case Difficulty.HARD: {
                this.number = Math.ceil(Math.random() * 5);
                break;
            }
        }
    }
}