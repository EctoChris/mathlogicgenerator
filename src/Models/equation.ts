enum Difficulty {
    JUNIOR = "JUNIOR",
    MEDIUM = "MEDIUM",
    HARD = "HARD"
}

export class Equation {
    public objects:any = [];
    public operators:any = [];
    public difficulty:Difficulty;
    public answer:any;
    public isProblem:boolean;
    
    constructor(array, difficulty, isFirst, isSecondOrThird, isProblem){
        this.objects = array
        this.difficulty = difficulty;
        this.setupOperators(isFirst, isSecondOrThird);
        this.solveEquation();
        this.isProblem = isProblem;
    }

    setupOperators(isFirst, isSecondOrThird){
        switch(this.difficulty){
            case Difficulty.JUNIOR: {
                for(let i:number = 0; i < this.objects.length-1; i++){
                    //Randomly pick operators from + or -
                    let r:number = Math.floor(Math.random() * 2);
                    if(isFirst){
                      r = 0;
                    }
                    if(r == 0){
                        this.operators.push('+');
                    } else {
                        this.operators.push('-');
                    }
                }
                break;
            }
            case Difficulty.MEDIUM: {
                for(let i:number = 0; i < this.objects.length-1; i++){
                    //Randomly pick operators from + or -
                    let r:number = Math.floor(Math.random() * 3);
                    if(isFirst){
                      r = 0;
                    }
                    switch(r){
                        case 0: 
                             this.operators.push('+');
                             break;
                        case 1: 
                            this.operators.push('-');
                            break;
                        case 2:
                            this.operators.push('*');
                            break;
                    }
                }
                break;
            }
            case Difficulty.HARD:{
                for(let i:number = 0; i < this.objects.length-1; i++){
                    //Randomly pick operators from + or -
                    let r:number = Math.floor(Math.random() * 3);
                    if(isFirst){
                      r = 0;
                    }
                    if(isSecondOrThird){
                        let r:number = Math.floor(Math.random() * 9);
                        switch(r){
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                                this.operators.push('+');
                                break;
                            case 6:
                            case 7:
                            case 8:
                                this.operators.push('*');
                                break;
                            default:
                                console.log('wtfffff');
                                break;
                        }
                    }
                    else {
                        switch(r){
                            case 0: 
                                 this.operators.push('+');
                                 break;
                            case 1: 
                                this.operators.push('-');
                                break;
                            case 2:
                                this.operators.push('*');
                                break;
                        }
                    }
                }
                break;
            }
        }
    }

    solveEquation(){
        let endEquationString:string = "";
        for(let i:number = 0; i < this.objects.length; i++){
            endEquationString += this.objects[i].number;
            if(i != this.objects.length -1){
                endEquationString += ' ' + this.operators[i] + ' ';
            }
        }
        console.log(endEquationString);
        this.answer = eval(endEquationString);
        console.log('answer = ', this.answer);
    }
}