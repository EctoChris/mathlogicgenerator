export class Letter {
    public char:string;
    public number:number;

    constructor(char, num){
        this.char = char;
        this.number = num;
    }
}