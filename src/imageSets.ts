export default {
  FRUITS: {
        APPLE: {
            SINGLE: "../assets/fruits/Apple.png",
            DOUBLE: "../assets/fruits/AppleDouble.png",
            TRIPLE: "../assets/fruits/AppleTriple.png"
        },
        BANANA: {
            SINGLE: "../assets/fruits/Banana.png",
            DOUBLE: "../assets/fruits/BananaDouble.png",
            TRIPLE: "../assets/fruits/BananaTriple.png"
        },
        BLUEBERRY: {
            SINGLE: "../assets/fruits/Blueberry.png",
            DOUBLE: "../assets/fruits/BlueberryDouble.png",
            TRIPLE: "../assets/fruits/BlueberryTriple.png"
        },
        CHERRY: {
            SINGLE: "../assets/fruits/Cherry.png",
            DOUBLE: "../assets/fruits/CherryDouble.png",
            TRIPLE: "../assets/fruits/CherryTriple.png"
        },
        GRAPES: {
            SINGLE: "../assets/fruits/Grapes.png",
            DOUBLE: "../assets/fruits/GrapesDouble.png",
            TRIPLE: "../assets/fruits/GrapesTriple.png",
        },
        KIWI: {
            SINGLE: "../assets/fruits/Kiwi.png",
            DOUBLE: "../assets/fruits/KiwiDouble.png",
            TRIPLE: "../assets/fruits/KiwiTriple.png",
        },
        LEMON: {
            SINGLE: "../assets/fruits/Lemon.png",
            DOUBLE: "../assets/fruits/LemonDouble.png",
            TRIPLE: "../assets/fruits/LemonTriple.png",
        },
        ORANGE:  {
            SINGLE:  "../assets/fruits/Orange.png",
            DOUBLE: "../assets/fruits/OrangeDouble.png",
            TRIPLE: "../assets/fruits/OrangeTriple.png",
        },
        PEAR: {
            SINGLE: "../assets/fruits/Pear.png",
            DOUBLE: "../assets/fruits/PearDouble.png",
            TRIPLE: "../assets/fruits/PearTriple.png",
        },
        PINEAPPLE: { 
            SINGLE: "../assets/fruits/Pineapple.png",
            DOUBLE: "../assets/fruits/PineappleDouble.png",
            TRIPLE: "../assets/fruits/PineappleTriple.png",
        },
        STRAWBERRY: {
            SINGLE: "../assets/fruits/Strawberry.png",
            DOUBLE: "../assets/fruits/StrawberryDouble.png",
            TRIPLE: "../assets/fruits/StrawberryTriple.png",
        },
        WATERMELON: {
            SINGLE:  "../assets/fruits/Watermelon.png",
            DOUBLE: "../assets/fruits/WatermelonDouble.png",
            TRIPLE: "../assets/fruits/WatermelonTriple.png",
        }
    }
}
    